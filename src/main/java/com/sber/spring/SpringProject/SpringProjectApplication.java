package com.sber.spring.SpringProject;

import com.sber.spring.SpringProject.dbexample.dao.BookDao;
import com.sber.spring.SpringProject.dbexample.dao.UserDao;
import com.sber.spring.SpringProject.dbexample.model.Book;
import com.sber.spring.SpringProject.dbexample.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Timestamp;


@SpringBootApplication
@Slf4j

public class SpringProjectApplication implements CommandLineRunner {


	private BookDao bookDao;
	private UserDao userDao;
	@Autowired
	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}



	public static void main(String[] args) {
		SpringApplication.run(SpringProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		User user = new User();
		user.setFirstName("Олег");
		user.setLastName("Петров");
		user.setPhone("+70000000003");
		Integer[] idBooksToUser = {4,4};
		user.setBookIds(idBooksToUser);


		System.out.println(bookDao.getAllObjects().toString());
		System.out.println(bookDao.getOneObjectById(4).toString());
		Book book = new Book();
		book.setAuthor("Test");
		book.setTitle("Test");
		Timestamp time = new Timestamp(2000);
		book.setDateAdded(time);


		bookDao.deleteObjectById(7);
//		bookDao.update(book,8);
//		bookDao.create(book);
		System.out.println(userDao.getListBookOfUser("+70000000000").toString());
//		userDao.create(user);

	}
}

