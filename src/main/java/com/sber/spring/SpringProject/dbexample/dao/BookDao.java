package com.sber.spring.SpringProject.dbexample.dao;

import com.sber.spring.SpringProject.dbexample.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;


import java.sql.SQLException;
import java.util.List;


@Slf4j
@Component
public class BookDao {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final String GET_BOOK_BY_ID_QUERY = """
            select * from books where id = :id
            """;
    private static final String GET_BOOKS_LIST_QUERY = """
            select * from books
            """;
    private static final String GET_BOOK_CREATE = """
            INSERT INTO books(author, title, date_added) VALUES (:author, :title, :date_added)
            """;
    private static final String GET_BOOK_UPDATE = """
            UPDATE books SET author = :author, title = :title, date_added = :date_added WHERE id = :id
            """;
    private static final String GET_BOOK_DELETE_BY_ID = """
            DELETE from books where id = :id
            """;

    public Book getOneObjectById(Integer id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
        return jdbcTemplate.queryForObject(GET_BOOK_BY_ID_QUERY,namedParameters,
                (rs, rowNum) -> new Book(
                        rs.getInt("id"),
                        rs.getString("author"),
                        rs.getString("title"),
                        rs.getDate("date_added")
                ));
    }

    public List<Book> getAllObjects() throws SQLException {
        return jdbcTemplate.query(GET_BOOKS_LIST_QUERY,
                (rs, rowNum) -> new Book(
                        rs.getInt("id"),
                        rs.getString("author"),
                        rs.getString("title"),
                        rs.getDate("date_added")
                ));
    }
    public void create(Book newBook) throws SQLException {
        SqlParameterSource paramSource = new MapSqlParameterSource()
                .addValue("author", newBook.getAuthor())
                .addValue("title", newBook.getTitle())
                .addValue("date_added", newBook.getDateAdded());
        jdbcTemplate.update(GET_BOOK_CREATE, paramSource);
    }
    public void deleteObjectById(Integer id) throws SQLException {
        SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
        jdbcTemplate.update(GET_BOOK_DELETE_BY_ID, paramSource);

    }
    public void update(Book updatedBook, Integer id){
        SqlParameterSource paramSource = new MapSqlParameterSource()
                .addValue("author", updatedBook.getAuthor())
                .addValue("title", updatedBook.getTitle())
                .addValue("date_added", updatedBook.getDateAdded())
                .addValue("id", id);
        jdbcTemplate.update(GET_BOOK_UPDATE, paramSource);
    }

}

