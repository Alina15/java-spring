CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    last_name VARCHAR NOT NULL,
    first_name VARCHAR NOT NULL,
    birth_date DATE,
    phone VARCHAR,
    email VARCHAR,
    book_ids INTEGER ARRAY
);