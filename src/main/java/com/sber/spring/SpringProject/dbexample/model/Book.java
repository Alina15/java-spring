package com.sber.spring.SpringProject.dbexample.model;

import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString

public class Book {

    private Integer id;
    private String title;
    private String author;
    private Date dateAdded;



}

