package com.sber.spring.SpringProject.dbexample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.spring.SpringProject.dbexample.contants.DataBaseConstants.*;

@ComponentScan
@Configuration
public class MyDataBaseContext {

    @Bean
    @Scope("prototype")
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME,
                DB_USER,
                DB_PASSWORD);
    }

}

