package com.sber.spring.SpringProject.dbexample.model;

import lombok.*;

import java.sql.Date;


@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private Integer id;
    private String lastName;
    private String firstName;
    private Date birthDate;
    private String phone;

    private String email;
    private Integer[] bookIds;


}

