package com.sber.spring.SpringProject.dbexample.dao;

import com.sber.spring.SpringProject.dbexample.model.Book;
import com.sber.spring.SpringProject.dbexample.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
@Slf4j
@Component
public class UserDao{
    private BookDao bookDao;
    @Autowired
    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    private static final String GET_USER_BY_ID_QUERY = """
            select * from users where id = :id
            """;
    private static final String GET_USER_LIST_QUERY = """
            select * from users
            """;

    private static final String USER_CREATE = """
            INSERT INTO users(last_name, first_name, birth_date, phone, email, book_ids) VALUES (:last_name, :first_name, :birth_date, :phone, :email, :book_ids )
            """;
    private static final String USER_UPDATE = """
            UPDATE users SET last_name = :last_name, first_name = ?, birth_date = ?, phone = ?, email = ?, book_ids = ? WHERE id = ?
            """;
    private static final String USER_DELETE_BY_ID = """
            DELETE from users where id = ?
            """;
    private static final String GET_BOOK_BY_PHONE = """
            select book_ids from users WHERE phone = :phone
            """;

    public User getOneObjectById(Integer id) {

        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);

        return  this.jdbcTemplate.queryForObject(GET_USER_BY_ID_QUERY,namedParameters,
                (rs, rowNum) ->{
                    User user = new User();
                    user.setId(rs.getInt("id"));
                user.setLastName(rs.getString("last_name"));
                user.setFirstName(rs.getString("first_name"));
                user.setBirthDate(rs.getDate("birth_date"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                Array e = rs.getArray("book_ids");
                user.setBookIds((Integer[])e.getArray());
                       return user;
        }

        );
    }

    public List<User> getAllObjects() {
        return this.jdbcTemplate.query(GET_USER_LIST_QUERY,
                (rs, rowNum) -> {
                    User user = new User();
                    user.setId(rs.getInt("id"));
                    user.setLastName(rs.getString("last_name"));
                    user.setFirstName(rs.getString("first_name"));
                    user.setBirthDate(rs.getDate("birth_date"));
                    user.setPhone(rs.getString("phone"));
                    user.setEmail(rs.getString("email"));
                    Array e = rs.getArray("book_ids");
                    user.setBookIds((Integer[])e.getArray());
                    return user;
                });

    }

    public void create(User newUser) {

        SqlParameterSource paramSource = new MapSqlParameterSource()
                .addValue("last_name", newUser.getLastName())
                .addValue("first_name", newUser.getFirstName())
                .addValue("birth_date", newUser.getBirthDate())
                .addValue("phone", newUser.getPhone())
                .addValue("email", newUser.getEmail())
               .addValue("book_ids", newUser.getBookIds());
        jdbcTemplate.update(USER_CREATE, paramSource);

    }

    public void update(User updatedUser, Integer id){
        SqlParameterSource paramSource = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("last_name", updatedUser.getLastName())
                .addValue("first_name", updatedUser.getFirstName())
                .addValue("birth_date", updatedUser.getBirthDate())
                .addValue("phone", updatedUser.getPhone())
                .addValue("email", updatedUser.getEmail())
                .addValue("book_ids", updatedUser.getBookIds());
        jdbcTemplate.update(USER_UPDATE, paramSource);
    }

    public void deleteObjectById(Integer id){
        SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
        jdbcTemplate.update(USER_DELETE_BY_ID, paramSource);

    }
    public List<Book> getListBookOfUser(String phone) {

        SqlParameterSource namedParameters = new MapSqlParameterSource("phone", phone);
        List<Book> booksToUser = new ArrayList<>();
        return this.jdbcTemplate.queryForObject(GET_BOOK_BY_PHONE,namedParameters,
                (rs, rowNum) ->{
                    User user = new User();
                    Array e = rs.getArray("book_ids");
                    user.setBookIds((Integer[])e.getArray());
                    if(user.getBookIds() != null){
                    for(Integer i: user.getBookIds()){
                booksToUser.add(bookDao.getOneObjectById(i));
                Book book = new Book();
                book.setId(i);
                booksToUser.add(book);
            }
                    return booksToUser;
                    }
                    else {
                        return null;
                    }

                }

        );
    }
}
