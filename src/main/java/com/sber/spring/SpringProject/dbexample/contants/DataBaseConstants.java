package com.sber.spring.SpringProject.dbexample.contants;

public interface DataBaseConstants {
    String DB_USER = "posgre";
    String DB_PASSWORD = "12345";
    String DB_HOST = "localhost";
    String DB_PORT = "5432";
    String DB_NAME = "spring_data";

}

